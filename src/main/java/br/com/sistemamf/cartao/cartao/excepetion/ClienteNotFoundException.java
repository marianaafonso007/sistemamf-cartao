package br.com.sistemamf.cartao.cartao.excepetion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O Cliente informado é inválido")
public class ClienteNotFoundException extends RuntimeException {
}

