package br.com.sistemamf.cartao.cartao.client;

import java.io.IOException;

public class ClienteClientFallback implements ClienteClient {
    private Exception cause;

    ClienteClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClienteDTO getById(Long id) {
        if(cause instanceof RuntimeException || cause instanceof IOException) {
            throw new RuntimeException("O serviço de cliente está offline. Favor contatar equipe de suporte.");
        }
        throw (RuntimeException) cause;
    }
}
