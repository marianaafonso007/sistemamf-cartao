package br.com.sistemamf.cartao.cartao.client;

import java.security.acl.LastOwnerException;

public class ClienteDTO {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
