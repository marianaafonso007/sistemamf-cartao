package br.com.sistemamf.cartao.cartao.service;

import br.com.sistemamf.cartao.cartao.client.ClienteClient;
import br.com.sistemamf.cartao.cartao.excepetion.CartaoAlreadyExistsException;
import br.com.sistemamf.cartao.cartao.excepetion.CartaoNotFoundException;
import br.com.sistemamf.cartao.cartao.model.Cartao;
import br.com.sistemamf.cartao.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao create(Cartao cartao) {
        clienteClient.getById(cartao.getClientId());
        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());
        if(byNumero.isPresent()) {
            throw new CartaoAlreadyExistsException();
        }
        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Optional<Cartao> byNumero = cartaoRepository.findByNumero(updatedCartao.getNumero());

        if(byNumero.isPresent()) {
            Cartao cartao = byNumero.get();
            cartao.setAtivo(updatedCartao.getAtivo());

            return cartaoRepository.save(cartao);
        }
        throw new CartaoNotFoundException();
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);
        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }
        return byId.get();
    }

}
