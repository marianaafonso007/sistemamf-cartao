package br.com.sistemamf.cartao.cartao.client;

import br.com.sistemamf.cartao.cartao.excepetion.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;


public class ClienteClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClienteNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}

