package br.com.sistemamf.cartao.cartao.model.dto.update;

public class UpdateCartaoRequest {

    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}