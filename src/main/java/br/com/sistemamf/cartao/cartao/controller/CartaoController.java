package br.com.sistemamf.cartao.cartao.controller;

import br.com.sistemamf.cartao.cartao.model.Cartao;
import br.com.sistemamf.cartao.cartao.model.dto.create.CreateCartaoRequest;
import br.com.sistemamf.cartao.cartao.model.dto.create.CreateCartaoResponse;
import br.com.sistemamf.cartao.cartao.model.dto.get.GetCartaoResponse;
import br.com.sistemamf.cartao.cartao.model.dto.get.GetIdCartaoResponse;
import br.com.sistemamf.cartao.cartao.model.dto.update.UpdateCartaoRequest;
import br.com.sistemamf.cartao.cartao.model.dto.update.UpdateCartaoResponse;
import br.com.sistemamf.cartao.cartao.model.mapper.CartaoMapper;
import br.com.sistemamf.cartao.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = CartaoMapper.fromCreateRequest(createCartaoRequest);
        cartao = cartaoService.create(cartao);
        return CartaoMapper.toCreateResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = CartaoMapper.fromUpdateRequest(updateCartaoRequest);
        cartao.setNumero(numero);
        cartao = cartaoService.update(cartao);
        return CartaoMapper.toUpdateResponse(cartao);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero) {
        Cartao byNumero = cartaoService.getByNumero(numero);
        return CartaoMapper.toGetResponse(byNumero);
    }

    @GetMapping("buscarId/{id}")
    public GetIdCartaoResponse getById(@PathVariable Long id) {
        Cartao cartao = cartaoService.getById(id);
        return CartaoMapper.toGetIdResponse(cartao);
    }
}
